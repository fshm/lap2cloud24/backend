from importlib import import_module
from core.settings import settings
import os


def get_metadata() -> list:
    project_dir = settings.BASE_DIR
    bind_metadata = []

    for file in project_dir.rglob("models.py"):
        try:
            module_path = str(file.relative_to(project_dir).with_suffix("")).replace(
                os.sep, "."
            )
            module = import_module(module_path)
            if hasattr(module, "metadata"):
                bind_metadata.append(module.metadata)
        except Exception as e:
            pass

    return bind_metadata


metadata = get_metadata()
