import re
from typing import Any


async def to_camel_case(key: str) -> str:
    exps: list = key.split("_")
    return "".join([x if x == exps[0] else x.capitalize() for x in exps])


async def to_snake_case(key: str) -> str:
    pattern: str = "[A-Z]"
    formatted_key: str = ""
    previousSpan: int = 0
    for exp in re.finditer(pattern, key):
        formatted_key += f"{key[previousSpan:exp.start()]}_{exp.group().lower()}"
        previousSpan = exp.end()
    formatted_key += key[previousSpan:]

    return formatted_key


async def change_type_case(data: dict[str, Any] | list, case: str) -> Any:
    formatted_data: dict[str, Any] | list = data
    if type(data) == dict:
        dict_data: dict = {}
        for [key, value] in data.items():
            if case == "sc":
                formatted_key: str = await to_snake_case(key)
            if case == "cc":
                formatted_key: str = await to_camel_case(key)
            if type(value) == dict:
                dict_data[formatted_key] = await change_type_case(value, case)
            else:
                dict_data[formatted_key] = value
        return dict_data
    else:
        return formatted_data
