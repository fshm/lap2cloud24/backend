from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker, declarative_base
from core.settings import settings
from utils.logger import Logger

logger = Logger("sqlalchemy.engine")

engine = create_engine(str(settings.SQLALCHEMY_DATABASE_URI))


# Log SQL queries
@event.listens_for(engine, "before_cursor_execute")
def log_sql(conn, cursor, statement, parameters, context, executemany):
    logger.debug("Executing Query", {"Query": statement})


SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


# Dependency to get the database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
