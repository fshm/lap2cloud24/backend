from typing import Any
from fastapi.templating import Jinja2Templates
from starlette.templating import _TemplateResponse
from fastapi import Request
from core.settings import settings

templates: Jinja2Templates = Jinja2Templates(directory=str(settings.TEMPLATE_DIRS))


async def render(
    request: Request, template: str, context: dict[str, Any] | None = dict()
) -> _TemplateResponse:
    return templates.TemplateResponse(request, template, context)
