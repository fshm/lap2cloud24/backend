from core.settings import settings
from typing import Any
from json import dumps
import logging


class DictFormatter(logging.Formatter):
    def format(self, record) -> str:
        data: dict[str, Any] = record.__dict__.copy()
        try:
            return dumps(data)
        except TypeError:
            data["args"] = str(data["args"])
            return dumps(data)


def Logger(__name__) -> logging.Logger:
    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    handler.setFormatter(DictFormatter())
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    if settings.DEBUG:
        logger.setLevel(logging.DEBUG)

    return logger
