from utils.db import Base
from sqlalchemy import Column, String, UUID, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import Mapped
from pydantic import UUID4, EmailStr
from uuid import uuid4
from datetime import datetime

# Alembic's target metadata
metadata = Base.metadata


# Your Models
class AdminMaster(Base):
    __tablename__ = "t_admin_master"
    access_id: Mapped[UUID4] = Column(UUID, primary_key=True, index=True)
    username: Mapped[str] = Column(String(50), unique=True)
    email: Mapped[EmailStr] = Column(String(100))
    password: Mapped[str] = Column(String(150))
    is_admin: Mapped[bool] = Column(Boolean, default=True)
    ins_ts: Mapped[DateTime] = Column(DateTime, default=datetime.now)
    upd_ts: Mapped[DateTime] = Column(
        DateTime, default=datetime.now, onupdate=datetime.now
    )
