import secrets
from typing import Any
from dotenv import load_dotenv
from pathlib import PosixPath, Path
from pydantic import (
    AnyHttpUrl,
    PostgresDsn,
    ValidationInfo,
    field_validator,
)
from pydantic_settings import BaseSettings, SettingsConfigDict


# Core Settings
class Settings(BaseSettings):
    load_dotenv()
    PROJECT_NAME: str
    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 24 * 60
    BACKEND_CORS_ORIGINS: list[AnyHttpUrl] | str = []

    @field_validator("BACKEND_CORS_ORIGINS", mode="before")
    @classmethod
    def assemble_cors_origins(cls, v: str | list[str]) -> list[str] | str:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, list | str):
            return v

    # DB SETTINGS
    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    SQLALCHEMY_DATABASE_URI: PostgresDsn | None = None

    @field_validator("SQLALCHEMY_DATABASE_URI", mode="before")
    def db_connection(cls, v: str | None, info: ValidationInfo) -> Any:
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql+psycopg",
            username=info.data.get("POSTGRES_USER"),
            password=info.data.get("POSTGRES_PASSWORD"),
            host=info.data.get("POSTGRES_SERVER"),
            path=info.data.get("POSTGRES_DB"),
        )

    # DIR MANAGEMENT
    BASE_DIR: PosixPath = Path(__file__).resolve().parent.parent
    TEMPLATE_DIRS: PosixPath = Path(BASE_DIR / "templates")
    STATIC_DIRS: PosixPath = Path(BASE_DIR / "static" / "public")

    DEBUG: bool

    @field_validator("DEBUG", mode="before")
    def debug(cls, v: str) -> Any:
        if isinstance(v, str):
            return bool(v)

    model_config = SettingsConfigDict(case_sensitive=True)


settings = Settings()
