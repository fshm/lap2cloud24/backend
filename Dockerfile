# Pull base image
FROM python:3.11-slim-bullseye

# Set environment variables
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV MODULE_NAME=lap2cloud
ENV VARIABLE_NAME=app
ENV WORKERS_PER_CORE=1
ENV MAX_WORKERS=1
ENV WEB_CONCURRENCY=1
ENV BIND=0.0.0.0:80

# Set working dir
WORKDIR /app

ADD . /app

RUN apt update -y
RUN apt upgrade -y
# Installing latest version of nodejs
RUN curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
RUN apt install -y build-essential vim curl nodejs

# Clean up the APT cache to reduce the image size
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Generating public assets
RUN npm install -g yarn
RUN yarn run build

# Installing dependencies
RUN pip install --no-cache-dir -r requirements.txt

# CMD bash entrypoint.sh
CMD ["gunicorn", "-k", "uvicorn.workers.UvicornWorker", "-c", "gunicorn_conf.py", "$MODULE_NAME:$VARIABLE_NAME"]
# CMD ["uvicorn", "lap2cloud:app", "--host", "0.0.0.0", "--port", "80"]
