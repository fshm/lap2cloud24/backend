from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from core.settings import settings
from pages.views import router as pagesRouter
from registration.views import router as registrationRouter


app = FastAPI()

# Router config
app.include_router(pagesRouter)
app.include_router(registrationRouter)

# Static files
app.mount("/static", StaticFiles(directory=str(settings.STATIC_DIRS)), name="static")
