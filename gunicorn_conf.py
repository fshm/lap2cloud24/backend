import multiprocessing
import os

workers_per_core_str = os.getenv("WORKERS_PER_CORE", "1")
web_concurrency_str = os.getenv("WEB_CONCURRENCY", None)
host = os.getenv("BIND", "0.0.0.0:8080")
use_max_workers = None
if web_concurrency_str:
    use_max_workers = int(web_concurrency_str)
    workers_per_core = 1
else:
    workers_per_core = float(workers_per_core_str)

cores = multiprocessing.cpu_count()
workers = max(int(workers_per_core * cores), 2)
if use_max_workers:
    workers = min(workers, use_max_workers)

accesslog = "-"
errorlog = "-"
bind = str(host)
worker_class = "uvicorn.workers.UvicornWorker"
workers = workers
