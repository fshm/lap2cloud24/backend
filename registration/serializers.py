from pydantic import BaseModel, UUID4, EmailStr, HttpUrl
from typing import Optional


# Your Serializers
class CreateRegistrationSerializer(BaseModel):
    user_id: UUID4
    first_name: str
    last_name: str
    sex: str
    food_preference: str
    phone: str
    email: EmailStr
    institution: str
    designation: str
    # transaction_id: str
    # portfolio_link: Optional[HttpUrl | None]
    # pon: str


class AccountMasterSerializer(BaseModel):
    user_id: UUID4
    first_name: str
    last_name: str
    sex: str
    phone: str
    email: EmailStr


class AccountDetailsSerializer(BaseModel):
    user_id: UUID4
    institution: str
    designation: str
    food_preference: str
    # portfolio_link: Optional[HttpUrl | None] = None
    # pon: str


class EventRegistrationSerializer(BaseModel):
    user_id: UUID4
    transaction_id: str
    event_name: str
    proof_of_payment: str
    verification_status: Optional[bool] = False
    verified_by: Optional[UUID4 | None] = None
