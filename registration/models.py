from utils.db import Base
from sqlalchemy import Column, String, UUID, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import Mapped
from pydantic import UUID4, EmailStr
from uuid import uuid4
from datetime import datetime

# Alembic's target metadata
metadata = Base.metadata


# Your Models
class AccountMaster(Base):
    __tablename__ = "t_cln_account_master"

    user_id: Mapped[UUID4] = Column(UUID, primary_key=True, index=True)
    first_name: Mapped[str] = Column(String(50))
    last_name: Mapped[str] = Column(String(50))
    sex: Mapped[str] = Column(String(10))
    email: Mapped[EmailStr] = Column(String(100))
    phone: Mapped[str] = Column(String(10))
    ins_ts: Mapped[DateTime] = Column(DateTime, default=datetime.now)
    upd_ts: Mapped[DateTime] = Column(
        DateTime, default=datetime.now, onupdate=datetime.now
    )


class AccountDetails(Base):
    __tablename__ = "t_cln_account_details"

    id: Mapped[UUID4] = Column(UUID, primary_key=True, index=True, default=uuid4)
    user_id: Mapped[UUID4] = Column(
        UUID(as_uuid=True), ForeignKey("t_cln_account_master.user_id"), index=True
    )
    institution: Mapped[str] = Column(String(200))
    designation: Mapped[str] = Column(String(50))
    food_preference: Mapped[str] = Column(String(50))
    portfolio_link: Mapped[str | None] = Column(String(255), default=None)
    pon: Mapped[str] = Column(String(50))
    ins_ts: Mapped[DateTime] = Column(DateTime, default=datetime.now)
    upd_ts: Mapped[DateTime] = Column(
        DateTime, default=datetime.now, onupdate=datetime.now
    )


class EventRegistration(Base):
    __tablename__ = "t_cln_event_registration"

    id: Mapped[UUID4] = Column(UUID, primary_key=True, index=True, default=uuid4)
    user_id: Mapped[UUID4] = Column(
        UUID(as_uuid=True), ForeignKey("t_cln_account_master.user_id"), index=True
    )
    event_name: Mapped[str | None] = Column(String(255), default=None)
    transaction_id: Mapped[str] = Column(String(255))
    proof_of_payment: Mapped[str] = Column(String(255), default="")
    verification_status: Mapped[bool] = Column(Boolean, default=False)
    verified_by: Mapped[UUID4 | None] = Column(
        UUID(as_uuid=True), ForeignKey("t_cln_account_master.user_id"), default=None
    )
    ins_ts: Mapped[DateTime] = Column(DateTime, default=datetime.now)
    upd_ts: Mapped[DateTime] = Column(
        DateTime, default=datetime.now, onupdate=datetime.now
    )
