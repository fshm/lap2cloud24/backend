from fastapi import APIRouter, Request, UploadFile, File
from fastapi.params import Depends
from sqlalchemy.orm import Session
from utils.db import get_db
from utils.logger import Logger
from utils.general import change_type_case
from .serializers import (
    CreateRegistrationSerializer,
    AccountMasterSerializer,
    AccountDetailsSerializer,
    EventRegistrationSerializer,
)
from .models import (
    AccountMaster,
    AccountDetails,
    EventRegistration,
)


router = APIRouter(prefix="")
logger = Logger(__name__)


# Views goes here
# @router.post("/register")
# async def create_registration(
#     request: Request,
#     file: UploadFile = File(...),
#     db: Session = Depends(get_db),
# ):
#     try:
#         logger.info("Onboarding new user.")

#         logger.info("Validating input data.")
#         # Serializing required data
#         request_data = await change_type_case(dict(await request.form()), "sc")
#         logger.debug("Formatted data to snake case", request_data)
#         form_data = CreateRegistrationSerializer(**request_data)
#         logger.debug("Input data", {"form_data": form_data, "filename": file.filename})
#         logger.info("Input data validated successfully.")

#         # Inserting data into DB
#         logger.info("Inserting user data into DB.")
#         logger.info("Beginning DBMS transaction.")

#         # Account master
#         logger.info("Inserting data into account master.")
#         db_account_master = AccountMasterSerializer(**form_data.model_dump())
#         insert_account_master = AccountMaster(**db_account_master.model_dump())
#         logger.debug("Account master data", db_account_master)
#         db.add(insert_account_master)
#         logger.info("Committing works for account master.")
#         db.commit()
#         logger.info("Work committed successfully.")
#         logger.info("Data inserted successfully.")

#         # Account Details
#         logger.info("Inserting data info account details.")
#         db_account_details = AccountDetailsSerializer(**form_data.model_dump())
#         insert_account_details = AccountDetails(**db_account_details.model_dump())
#         logger.debug("Account details data", db_account_details.model_dump())
#         db.add(insert_account_details)
#         logger.info("Data inserted successfully.")

#         # Event registration
#         logger.info("Inserting data for event registration.")
#         db_event_registration = EventRegistrationSerializer(
#             **form_data.model_dump(), event_name="UnlockTheCloud'24"
#         )
#         insert_event_registration = EventRegistration(
#             **db_event_registration.model_dump()
#         )
#         logger.debug("Event registration data", db_event_registration.model_dump())
#         db.add(insert_event_registration)
#         logger.info("Data inserted successfully.")

#         logger.info("Committing works for account details and event registration.")
#         db.commit()
#         logger.info("Work committed successfully.")

#         logger.info("User event registered successfully.")
#     except Exception as error:
#         logger.error("", error)
#         db.rollback()
#         raise error


@router.post("/register")
async def create_registration(
    request: Request,
    db: Session = Depends(get_db),
):
    try:
        logger.info("Onboarding new user.")

        logger.info("Validating input data.")
        # Serializing required data
        request_data = await change_type_case(dict(await request.form()), "sc")
        logger.debug("Formatted data to snake case", request_data)
        form_data = CreateRegistrationSerializer(**request_data)
        logger.debug("Input data", {"form_data": form_data})
        logger.info("Input data validated successfully.")

        # Inserting data into DB
        logger.info("Inserting user data into DB.")
        logger.info("Beginning DBMS transaction.")

        # Account master
        logger.info("Inserting data into account master.")
        db_account_master = AccountMasterSerializer(**form_data.model_dump())
        insert_account_master = AccountMaster(**db_account_master.model_dump())
        logger.debug("Account master data", db_account_master)
        db.add(insert_account_master)
        logger.info("Committing works for account master.")
        db.commit()
        logger.info("Work committed successfully.")
        logger.info("Data inserted successfully.")

        # Account Details
        logger.info("Inserting data info account details.")
        db_account_details = AccountDetailsSerializer(
            **form_data.model_dump(), pon = 'None'
            )
        insert_account_details = AccountDetails(**db_account_details.model_dump())
        logger.debug("Account details data", db_account_details.model_dump())
        db.add(insert_account_details)
        logger.info("Data inserted successfully.")

        # Event registration
        # logger.info("Inserting data for event registration.")
        # db_event_registration = EventRegistrationSerializer(
        #     **form_data.model_dump(), event_name="UnlockTheCloud'24"
        # )
        # insert_event_registration = EventRegistration(
        #     **db_event_registration.model_dump()
        # )
        # logger.debug("Event registration data", db_event_registration.model_dump())
        # db.add(insert_event_registration)
        # logger.info("Data inserted successfully.")

        logger.info("Committing works for account details and pre event registration.")
        db.commit()
        logger.info("Work committed successfully.")

        logger.info("User event registered successfully.")
    except Exception as error:
        logger.error("", error)
        db.rollback()
        raise error
