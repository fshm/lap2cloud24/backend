'use strict'

import axios from "axios"
import { v4 as uuidv4 } from 'uuid'
import {
  toastSuccess,
  toastError
} from './toastMessages.js'

let dataKey = uuidv4()
// const eventFormOne = document.querySelector('#formOne').addEventListener('submit', formOne)
const eventFormOne = document.querySelector('#formOne').addEventListener('submit', register)
const closeToast = (query) => document.querySelector(query).classList.add('hidden')

// const eventFormTwo = document.querySelector('#formTwo').addEventListener('submit', register)
// const eventPaymentScreen = document.querySelector('#payment_success').addEventListener('click', paymentSuccess)

function paymentSuccess() {
  document.querySelector("#formOne").classList.replace("block", "hidden")
  document.querySelector("#formTwo").classList.replace("hidden", "block")
}

function formOne(event) {

  // Prevent form action
  event.preventDefault()

  // Clear session storage if any data already exists
  // if (dataKey || sessionStorage.length) sessionStorage.clear()

  // Generating random UUID
  dataKey = uuidv4()

  // Extract form data
  const data = {
    userId: dataKey,
    firstName: document.querySelector("#first_name").value,
    lastName: document.querySelector("#last_name").value,
    sex: document.querySelector("#sex").value,
    foodPreference: document.querySelector("#food_preference").value,
    phone: document.querySelector("#phone").value,
    email: document.querySelector("#email").value,
    institution: document.querySelector("#institution").value,
    designation: document.querySelector("#designation").value,
  }

  // Toggle Payment Screen
  // document.querySelector('#payment_toggle').click()

  // Store data in session storage
  // sessionStorage.setItem(dataKey, JSON.stringify(data))

  // Temp Code
  return data
}

function formTwo(event) {

  // Prevent form action
  event.preventDefault()

  const formData = new FormData()

  // Extract form data
  const transactionData = {
    transactionId: document.querySelector("#transaction_id").value,
    file: document.querySelector("#file").files,
    portfolioLink: document.querySelector("#portfolio_link").value,
    pon: document.querySelector("#pon").value
  }

  // Appending as form data
  for (const [key, value] of Object.entries({
    ...JSON.parse(sessionStorage.getItem(dataKey)),
    ...transactionData
  })) formData.append(key, key === 'file' ? value[0] : value)

  return formData

}

async function register(event) {
  // Registration
  // const formData = formTwo(event)
  const formData = formOne(event)
  console.debug(formData, 'Form Data')

  let regStatus = null
  try {
    const response = await axios.post('/register', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    regStatus = response.status
    document.querySelector('#formOne').reset()
  } catch (error) {
    console.error(error)
  } finally {
    document.querySelector('#message-toast').innerHTML = regStatus === 200 ? toastSuccess() : toastError()
    // Simulate an HTTP redirect:
    if(regStatus===200) window.location.replace("/confirmation");
  }
}
