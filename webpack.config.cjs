const path = require('path/posix')

module.exports = {
  entry: path.resolve(__dirname, 'static/src/javascript/index.js'),
  target: 'web',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'static/public/javascript'),
    filename: 'index.min.js',
  }
};