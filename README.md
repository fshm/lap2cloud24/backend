# FSHM Event Web Portal

### File Structure:

```
.
├── app                  # "app" is a Python package
│   ├── __init__.py      # this file makes "app" a "Python package"
│   ├── models.py        # "models" module contains DB models, e.g. import app.models
│   ├── serializers.py   # "serializers" module contains serializers, e.g. import app.serializers
│   ├── tests.py         # "tests cases" for unit testing, etc. 
│   ├── urls.py          # makes "routers" a "Python module"
│   └── views.py         # make "handlers" a "Python module"
```


In Progress:
- Authentication
- Type Casing

TODO:
- SMTP connection
- Handle folder does not exists error for static and media folders
- fastapi managed command line utils
- TOTP
## Build, test and run

-  install docker and docker-compose
- create virtual environment
`python -m venv .venv`
`source .venv/bin/activate`
`pip install -r requirements.txt`
`cp .env.example .env`
`docker compose up -d`
`gunicorn -k uvicorn.workers.UvicornWorker -c gunicorn_conf.py lap2cloud:app`
