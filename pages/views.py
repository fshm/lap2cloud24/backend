from fastapi import Request, APIRouter
from fastapi.responses import HTMLResponse
from utils.rendering import render
from utils.logger import Logger


router = APIRouter(prefix="")
logger = Logger(__name__)


# Views goes here
@router.get("/", response_class=HTMLResponse)
async def page_home(request: Request):
    return await render(request, "index.html")


@router.get("/schedule", response_class=HTMLResponse)
async def page_schedule(request: Request):
    return await render(request, "schedule.html")


@router.get("/register", response_class=HTMLResponse)
async def page_register(request: Request):
    return await render(request, "register.html")

@router.get("/confirmation", response_class=HTMLResponse)
async def page_confirmation(request: Request):
    return await render(request, "confirm.html")
