from fastapi.testclient import TestClient
from lap2cloud import app

client = TestClient(app=app)


# Test cases goes here.
def test_homePage():
    response = client.get("/")
    assert response.status_code == 200


# def test_page_schedule():
#     response = client.get("/schedule")
#     assert response.status_code == 200


def test_page_register():
    response = client.get("/register")
    assert response.status_code == 200

def test_page_confirmation():
    response = client.get("/confirmation")
    assert response.status_code == 200