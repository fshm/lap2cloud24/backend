/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './templates/**/*.html',
    './static/**/*.js'
  ],
  safelist: ["hidden"],
  theme: {
    extend: {},
  },
  plugins: [],
}

